﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace LetterChaos
{
    partial class LetterChaos : Form
    {
        private RichTextBox rtb;
        TextBox tb = new TextBox();
        InstalledFontCollection installedFontCollection = new InstalledFontCollection();
        private List<FontFamily> fonts;
        int fonti = 0;
        public LetterChaos()
        {
            InitializeComponent();
            rtb = output;
            fonts = new List<FontFamily>();
            fonts.AddRange(installedFontCollection.Families);
        }

        void b_Click(object sender, EventArgs e)
        {
            
            for (int i = 0; i < rtb.TextLength; i++)
            {
                rtb.Select(i, rtb.TextLength);
                SetFont(rtb, 15);
            }

            Console.WriteLine(fonts.Count);

        }
        public void SetFont(RichTextBox rtb, float fontSize)
        {
            
            if (rtb.SelectionLength > 0)
            {
                int selStart = rtb.SelectionStart;
                int selLength = rtb.SelectionLength;
                int i;
                for (i = 0; i <= selLength - 1; i++)
                {
                    if (fonti>=fonts.Count)
                    {
                        fonti = 0;
                    }
                    
                    rtb.Select(selStart + i, 1);
                    rtb.SelectionFont = new System.Drawing.Font(fonts[fonti], fontSize,
                        FontStyle.Regular);
                }
                rtb.Select(selStart, selLength);
                fonti++;
            }
        }
    }
}