﻿using System;
using System.Drawing;

namespace Flags
{
    class Country
    {
        private String name;
        private Image image;
        
        public Country(String name, Image image)
        {
            this.name = name;
            this.image = image;
        }
        public String getName()
        {
            return name;
        }

        public Image getImage()
        {
            return image;
        }
    }
}
