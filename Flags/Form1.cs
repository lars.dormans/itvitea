﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flags.Properties;

namespace Flags
{
    public partial class Form1 : Form
    {
        private Country activeCountry;
        private List<Country> countryList = new List<Country>();
        private static Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
            ResourceManager resourceManager = new ResourceManager(typeof(Resources));
            ResourceSet rs = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in rs)
            {
                string key = entry.Key.ToString();
                object img = entry.Value;
                string count = key.Split('-')[1];
                countryList.Add(new Country(count, img as Image));
            }
            initDropdown();
            newImage();
        }
        private void newImage()
        {
            int r = rnd.Next(countryList.Count());
            Country c = countryList[r];
            activeCountry = c;
            Image img = c.getImage();
            flag.Image = img;
        }
        private void initDropdown()
        {
            countryList.ForEach((country) =>
            {
                countries.Items.Add(country.getName());
            });
        }

        private void submit_Click(object sender, EventArgs e)
        {
            if (countries.SelectedItem.Equals(activeCountry.getName()))
            {
                MessageBox.Show("Correct");
                newImage();
            }
            else
            {
                MessageBox.Show("Incorrect, Try again");
            }
        }
    }
}
