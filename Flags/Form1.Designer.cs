﻿namespace Flags
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flag = new System.Windows.Forms.PictureBox();
            this.countries = new System.Windows.Forms.ComboBox();
            this.submit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.flag)).BeginInit();
            this.SuspendLayout();
            // 
            // flag
            // 
            this.flag.BackColor = System.Drawing.SystemColors.Control;
            this.flag.Location = new System.Drawing.Point(22, 12);
            this.flag.Name = "flag";
            this.flag.Size = new System.Drawing.Size(512, 512);
            this.flag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.flag.TabIndex = 0;
            this.flag.TabStop = false;
            // 
            // countries
            // 
            this.countries.FormattingEnabled = true;
            this.countries.Location = new System.Drawing.Point(196, 530);
            this.countries.Name = "countries";
            this.countries.Size = new System.Drawing.Size(165, 24);
            this.countries.TabIndex = 1;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(241, 560);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(75, 23);
            this.submit.TabIndex = 2;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 622);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.countries);
            this.Controls.Add(this.flag);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.flag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox flag;
        private System.Windows.Forms.ComboBox countries;
        private System.Windows.Forms.Button submit;
    }
}

