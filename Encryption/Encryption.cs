﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Encryption
{
    public partial class Encryption : Form
    {
        public Encryption()
        {
            InitializeComponent();
        }
        //Create keys
        private void button2_Click(object sender, EventArgs e)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            String priv = RSAUtil.ExportPrivateKey(rsa);
            String pub = RSAUtil.ExportPublicKey(rsa);
            SaveFileDialog savePriv = new SaveFileDialog
            {
                FileName = "private key.txt",
                Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (savePriv.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(savePriv.FileName, priv);
            }
            SaveFileDialog savePub = new SaveFileDialog();
            savePub.FileName = "public key.txt";
            savePub.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            savePub.FilterIndex = 2;
            savePub.RestoreDirectory = true;

            if (savePub.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(savePub.FileName, pub);
            }
        }
        //Encrypt/Decrypt
        private void button1_Click(object sender, EventArgs e)
        {
            bool priv = privB.Checked ? true : false;
            if (keyinput.Text.Length > 0 && textInput.Text.Length > 0)
            {
                if (priv)
                {
                    try
                    {
                        RSACryptoServiceProvider csp = RSAUtil.ImportPrivateKey(keyinput.Text);
                        byte[] textBytes = Convert.FromBase64String(textOutput.Text);
                        byte[] decBytes = csp.Decrypt(textBytes, true);
                        String decrypt = Encoding.ASCII.GetString(decBytes);
                        textOutput.Text = decrypt;
                    }
                    catch (CryptographicException exception)
                    {
                        MessageBox.Show("Invalid input");
                    }
                    catch (NullReferenceException exe)
                    {
                        MessageBox.Show("Invalid key");
                    }
                    catch (InvalidCastException exe)
                    {
                        MessageBox.Show("Invalid keymode");
                    }
                }
                else
                {
                    try
                    {
                        RSACryptoServiceProvider csp = RSAUtil.ImportPublicKey(keyinput.Text);
                        byte[] textByes = Encoding.ASCII.GetBytes(textInput.Text);
                        byte[] encBytes = csp.Encrypt(textByes, true);
                        textOutput.Text = Convert.ToBase64String(encBytes);
                    }
                    catch (InvalidCastException exe)
                    {
                        MessageBox.Show("Invalid key mode");
                    }
                }
            }
        }
    }
}
