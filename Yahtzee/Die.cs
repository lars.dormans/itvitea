﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yahtzee
{
    class Die
    {
        private static Random rnd;
        private int number;
        private Boolean locked;
        private int sides = 6;
        private Label label;

        public Die(Label label)
        {
            this.label = label;
            rnd = new Random();
        }

        public void Roll()
        {
            if (!locked)
            {
                number = rnd.Next(1, sides + 1);
                label.Text = number.ToString();
            }
        }

        public int Number => number;

        public bool Locked
        {
            get => locked;
            set => locked = value;
        }
    }
}
