﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yahtzee
{
    public partial class Yahtzee : Form
    {
        private DiceHolder holder;
        public Yahtzee()
        {
            InitializeComponent();
            List<Die> dices = new List<Die>();
            dices.Add(new Die(dice1));
            dices.Add(new Die(dice2));
            dices.Add(new Die(dice3));
            dices.Add(new Die(dice4));
            dices.Add(new Die(dice5));
            holder = new DiceHolder(dices);
        }

        private void dice1_Click(object sender, EventArgs e)
        {
            if (holder.Dices.ElementAt(0).Locked)
            {
                dice1.ForeColor = Color.Black;
                holder.Unhold(0);
            }
            else
            {
                dice1.ForeColor = Color.Red;
                holder.Hold(0);
            }
        }

        private void dice2_Click(object sender, EventArgs e)
        {
            if (holder.Dices.ElementAt(1).Locked)
            {
                dice2.ForeColor = Color.Black;
                holder.Unhold(1);
            }
            else
            {
                dice2.ForeColor = Color.Red;
                holder.Hold(1);
            }
        }

        private void dice3_Click(object sender, EventArgs e)
        {
            if (holder.Dices.ElementAt(2).Locked)
            {
                dice3.ForeColor = Color.Black;
                holder.Unhold(2);
            }
            else
            {
                dice3.ForeColor = Color.Red;
                holder.Hold(2);
            }
        }

        private void dice4_Click(object sender, EventArgs e)
        {
            if (holder.Dices.ElementAt(3).Locked)
            {
                dice4.ForeColor = Color.Black;
                holder.Unhold(3);
            }
            else
            {
                dice4.ForeColor = Color.Red;
                holder.Hold(3);
            }
        }

        private void dice5_Click(object sender, EventArgs e)
        {
            if (holder.Dices.ElementAt(4).Locked)
            {
                dice5.ForeColor = Color.Black;
                holder.Unhold(4);
            }
            else
            {
                dice5.ForeColor = Color.Red;
                holder.Hold(4);
            }
        }

        private void roll_Click(object sender, EventArgs e)
        {
            holder.Roll();
        }
    }
}
