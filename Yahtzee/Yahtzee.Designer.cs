﻿namespace Yahtzee
{
    partial class Yahtzee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dice1 = new System.Windows.Forms.Label();
            this.dice2 = new System.Windows.Forms.Label();
            this.dice3 = new System.Windows.Forms.Label();
            this.dice4 = new System.Windows.Forms.Label();
            this.dice5 = new System.Windows.Forms.Label();
            this.roll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dice1
            // 
            this.dice1.AutoSize = true;
            this.dice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dice1.Location = new System.Drawing.Point(12, 9);
            this.dice1.Name = "dice1";
            this.dice1.Size = new System.Drawing.Size(36, 39);
            this.dice1.TabIndex = 0;
            this.dice1.Text = "0";
            this.dice1.Click += new System.EventHandler(this.dice1_Click);
            // 
            // dice2
            // 
            this.dice2.AutoSize = true;
            this.dice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dice2.Location = new System.Drawing.Point(55, 9);
            this.dice2.Name = "dice2";
            this.dice2.Size = new System.Drawing.Size(36, 39);
            this.dice2.TabIndex = 1;
            this.dice2.Text = "0";
            this.dice2.Click += new System.EventHandler(this.dice2_Click);
            // 
            // dice3
            // 
            this.dice3.AutoSize = true;
            this.dice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dice3.Location = new System.Drawing.Point(97, 9);
            this.dice3.Name = "dice3";
            this.dice3.Size = new System.Drawing.Size(36, 39);
            this.dice3.TabIndex = 2;
            this.dice3.Text = "0";
            this.dice3.Click += new System.EventHandler(this.dice3_Click);
            // 
            // dice4
            // 
            this.dice4.AutoSize = true;
            this.dice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dice4.Location = new System.Drawing.Point(139, 9);
            this.dice4.Name = "dice4";
            this.dice4.Size = new System.Drawing.Size(36, 39);
            this.dice4.TabIndex = 3;
            this.dice4.Text = "0";
            this.dice4.Click += new System.EventHandler(this.dice4_Click);
            // 
            // dice5
            // 
            this.dice5.AutoSize = true;
            this.dice5.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dice5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dice5.Location = new System.Drawing.Point(181, 9);
            this.dice5.Name = "dice5";
            this.dice5.Size = new System.Drawing.Size(36, 39);
            this.dice5.TabIndex = 4;
            this.dice5.Text = "0";
            this.dice5.Click += new System.EventHandler(this.dice5_Click);
            // 
            // roll
            // 
            this.roll.Location = new System.Drawing.Point(19, 51);
            this.roll.Name = "roll";
            this.roll.Size = new System.Drawing.Size(198, 23);
            this.roll.TabIndex = 5;
            this.roll.Text = "Roll";
            this.roll.UseVisualStyleBackColor = true;
            this.roll.Click += new System.EventHandler(this.roll_Click);
            // 
            // Yahtzee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.roll);
            this.Controls.Add(this.dice5);
            this.Controls.Add(this.dice4);
            this.Controls.Add(this.dice3);
            this.Controls.Add(this.dice2);
            this.Controls.Add(this.dice1);
            this.Name = "Yahtzee";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dice1;
        private System.Windows.Forms.Label dice2;
        private System.Windows.Forms.Label dice3;
        private System.Windows.Forms.Label dice4;
        private System.Windows.Forms.Label dice5;
        private System.Windows.Forms.Button roll;
    }
}

