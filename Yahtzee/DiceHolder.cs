﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
    class DiceHolder
    {
        public virtual List<Die> Dices { get; private set; }
        private const int MaxRolls = 3;
        public int RollCount { get; private set; }

        public DiceHolder(List<Die> dices)
        {
            RollCount = 0;
            if (dices.Count != 5) throw new ArgumentOutOfRangeException("dice", "Atleast 5 dices need to be provided");
            this.Dices = dices;
        }

        public List<Die> Roll()
        {
            if (RollCount == MaxRolls)
            {
                return null;
            }

            foreach (var die in Dices)
            {
                die.Roll();
            }

            RollCount++;
            return Dices;
        }

        public void Hold(int diceNumber)
        {
            Dices.ElementAt(diceNumber).Locked = true;
        }

        public void Unhold(int diceNumber)
        {
            Dices.ElementAt(diceNumber).Locked = false;
        }

    }
}
