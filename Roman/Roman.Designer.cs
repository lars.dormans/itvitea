﻿namespace Roman
{
    partial class Roman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.one = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.ten = new System.Windows.Forms.Button();
            this.fifty = new System.Windows.Forms.Button();
            this.hundred = new System.Windows.Forms.Button();
            this.fivehundred = new System.Windows.Forms.Button();
            this.thousand = new System.Windows.Forms.Button();
            this.input = new System.Windows.Forms.TextBox();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.equals = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(12, 12);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(75, 23);
            this.one.TabIndex = 0;
            this.one.Text = "I";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // five
            // 
            this.five.Location = new System.Drawing.Point(12, 41);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(75, 23);
            this.five.TabIndex = 1;
            this.five.Text = "V";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // ten
            // 
            this.ten.Location = new System.Drawing.Point(12, 70);
            this.ten.Name = "ten";
            this.ten.Size = new System.Drawing.Size(75, 23);
            this.ten.TabIndex = 2;
            this.ten.Text = "X";
            this.ten.UseVisualStyleBackColor = true;
            this.ten.Click += new System.EventHandler(this.ten_Click);
            // 
            // fifty
            // 
            this.fifty.Location = new System.Drawing.Point(12, 99);
            this.fifty.Name = "fifty";
            this.fifty.Size = new System.Drawing.Size(75, 23);
            this.fifty.TabIndex = 3;
            this.fifty.Text = "L";
            this.fifty.UseVisualStyleBackColor = true;
            this.fifty.Click += new System.EventHandler(this.fifty_Click);
            // 
            // hundred
            // 
            this.hundred.Location = new System.Drawing.Point(12, 128);
            this.hundred.Name = "hundred";
            this.hundred.Size = new System.Drawing.Size(75, 23);
            this.hundred.TabIndex = 4;
            this.hundred.Text = "C";
            this.hundred.UseVisualStyleBackColor = true;
            this.hundred.Click += new System.EventHandler(this.hundred_Click);
            // 
            // fivehundred
            // 
            this.fivehundred.Location = new System.Drawing.Point(12, 157);
            this.fivehundred.Name = "fivehundred";
            this.fivehundred.Size = new System.Drawing.Size(75, 23);
            this.fivehundred.TabIndex = 5;
            this.fivehundred.Text = "D";
            this.fivehundred.UseVisualStyleBackColor = true;
            this.fivehundred.Click += new System.EventHandler(this.fivehundred_Click);
            // 
            // thousand
            // 
            this.thousand.Location = new System.Drawing.Point(12, 186);
            this.thousand.Name = "thousand";
            this.thousand.Size = new System.Drawing.Size(75, 23);
            this.thousand.TabIndex = 6;
            this.thousand.Text = "M";
            this.thousand.UseVisualStyleBackColor = true;
            this.thousand.Click += new System.EventHandler(this.thousand_Click);
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(93, 12);
            this.input.Name = "input";
            this.input.ReadOnly = true;
            this.input.Size = new System.Drawing.Size(129, 20);
            this.input.TabIndex = 7;
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(118, 38);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 8;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(118, 67);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 23);
            this.minus.TabIndex = 9;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(118, 96);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(75, 23);
            this.multiply.TabIndex = 10;
            this.multiply.Text = "*";
            this.multiply.UseVisualStyleBackColor = true;
            this.multiply.Click += new System.EventHandler(this.multiply_Click);
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(118, 125);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(75, 23);
            this.divide.TabIndex = 11;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            this.divide.Click += new System.EventHandler(this.divide_Click);
            // 
            // equals
            // 
            this.equals.Location = new System.Drawing.Point(118, 157);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(75, 23);
            this.equals.TabIndex = 12;
            this.equals.Text = "=";
            this.equals.UseVisualStyleBackColor = true;
            this.equals.Click += new System.EventHandler(this.equals_Click);
            // 
            // Roman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 233);
            this.Controls.Add(this.equals);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.input);
            this.Controls.Add(this.thousand);
            this.Controls.Add(this.fivehundred);
            this.Controls.Add(this.hundred);
            this.Controls.Add(this.fifty);
            this.Controls.Add(this.ten);
            this.Controls.Add(this.five);
            this.Controls.Add(this.one);
            this.Name = "Roman";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button ten;
        private System.Windows.Forms.Button fifty;
        private System.Windows.Forms.Button hundred;
        private System.Windows.Forms.Button fivehundred;
        private System.Windows.Forms.Button thousand;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button equals;
    }
}

