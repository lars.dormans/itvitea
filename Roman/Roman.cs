﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roman
{
    public partial class Roman : Form
    {
        private int _result = 0;

        public int Result
        {
            get => _result;
            set => _result = value;
        }

        private bool _modifierPressed;

        private char _modifier;

        public bool ModifierPressed
        {
            get => _modifierPressed;
            set => _modifierPressed = value;
        }

        public char Modifier
        {
            get => _modifier;
            set => _modifier = value;
        }

        public Roman()
        {
            InitializeComponent();
        }

        private void one_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("I");
        }

        private void five_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("V");
        }

        private void ten_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("X");
        }

        private void fifty_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("L");
        }

        private void hundred_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("C");
        }

        private void fivehundred_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("D");
        }

        private void thousand_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("M");
        }

        private void plus_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '+';
        }

        private void minus_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '-';
        }

        private void multiply_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '*';
        }

        private void divide_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '/';
        }

        private void equals_Click(object sender, EventArgs e)
        {
            switch (Modifier)
            {
                case '+':
                    Result += ConvertToInt(input.Text);
                    break;
                case '-':
                    Result -= ConvertToInt(input.Text);
                    break;
                case '*':
                    Result *= ConvertToInt(input.Text);
                    break;
                case '/':
                    Result /= ConvertToInt(input.Text);
                    break;
                default:
                    Result = ConvertToInt(input.Text);
                    break;
            }
            Console.WriteLine(Result);
            input.Text = ToRoman(Result);
            Modifier = ' ';
            ModifierPressed = true;
        }

        private static int ConvertToInt(String roman)
        {
            int result = 0;
            foreach (var c in roman)
            {
                switch (c)
                {
                    case 'I':
                        result += 1;
                        break;
                    case 'V':
                        result += 5;
                        break;
                    case 'X':
                        result += 10;
                        break;
                    case 'L':
                        result += 50;
                        break;
                    case 'C':
                        result += 100;
                        break;
                    case 'D':
                        result += 500;
                        break;
                    case 'M':
                        result += 1000;
                        break;
                }
            }
            return result;
        }
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }
    }
}
